﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace TDDExercise
{
	public class StringCalculator
	{
		private const string DelimiterPattern = @"\[.*?\]";
		private const string LeftBracket = "[";
		private const string RightBracket = "]";
		private const string UnixNewLine = "\n";
		private const string DefaultDelimiter = ",";
		private const string ProvidedDelimiterMarker = "//";

		public int Add( string value )
		{
			if ( string.IsNullOrEmpty( value ) )
			{
				return 0;
			}

			var numbersSection = value;
			var delimiters = new List<string>( ) { DefaultDelimiter };
			if ( value.StartsWith( ProvidedDelimiterMarker ) )
			{
				var newLineIndex = value.IndexOf( UnixNewLine, StringComparison.Ordinal );
				var markerLength = ProvidedDelimiterMarker.Length;

				var delimiterSection = value.Substring( markerLength, newLineIndex - markerLength );
				delimiters = ParseDelimiters( delimiterSection );

				numbersSection = value.Substring( newLineIndex );
			}

			delimiters.Add( Environment.NewLine );
			delimiters.Add( UnixNewLine );

			var numbers = numbersSection.Split( delimiters.ToArray( ), StringSplitOptions.RemoveEmptyEntries );

			var parsedNumbers = ParseNumbers( numbers );

			CheckForNegativeNumbers( parsedNumbers );

			return parsedNumbers
				.Where( x => x <= 1000 )
				.Sum( );
		}

		private static List<string> ParseDelimiters( string delimiterSection )
		{
			if ( !delimiterSection.StartsWith( LeftBracket ) )
			{
				return new List<string>( ) { delimiterSection };
			}

			return Regex.Matches( delimiterSection, DelimiterPattern )
				.Select( match => match.Value
					.Replace( LeftBracket, string.Empty )
					.Replace( RightBracket, string.Empty ) )
				.ToList( );
		}

		private static List<int> ParseNumbers( IEnumerable<string> numbers )
		{
			var parsedNumbers = numbers.Select( x =>
			 {
				 int.TryParse( x, out var integer );
				 return integer;
			 } ).ToList( );
			return parsedNumbers;
		}

		private static void CheckForNegativeNumbers( List<int> parsedNumbers )
		{
			var negativeNumbers = parsedNumbers.Where( x => x < 0 ).ToList( );
			if ( negativeNumbers.Any( ) )
			{
				throw new Exception( "negatives not allowed " + string.Join( ",", negativeNumbers ) );
			}
		}
	}
}