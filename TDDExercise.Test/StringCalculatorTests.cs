﻿using System;
using System.Collections.Generic;
using Xunit;

namespace TDDExercise.Test
{
	public class StringCalculatorTests
	{
		[Theory]
		[InlineData( "", 0 )]
		[InlineData( "1", 1 )]
		[InlineData( "1,2", 3 )]
		[InlineData( "3\r\n4", 7 )]
		[InlineData( "1\n2,3", 6 )]
		[InlineData( "2,1000", 1002 )]
		[InlineData( "2,1001", 2 )]
		public void Add_Should_AddTheNumbersTogether( string inputValue, int expectedResult )
		{
			// Given
			var calculator = new StringCalculator( );

			// When
			var result = calculator.Add( inputValue );

			// Then
			Assert.Equal( expectedResult, result );
		}

		[Fact]
		public void Add_Should_AddAnUnknownAmountOfNumbersTogether( )
		{
			// Given
			var expectedResult = 100;
			var inputValue = GenerateAnInputValueThatAddsUpTo( expectedResult );

			var calculator = new StringCalculator( );

			// When
			var result = calculator.Add( inputValue );

			// Then
			Assert.Equal( expectedResult, result );
		}

		[Theory]
		[InlineData( "//;\n5;6", 11 )]
		[InlineData( "//!\n7!8", 15 )]
		[InlineData( "//\\\n9\\10", 19 )]
		[InlineData( "//[--]\n16--17", 33 )]
		[InlineData( "//[-][%]\n18-19%20", 57 )]
		[InlineData( "//[---][!]\n21---22!23", 66 )]
		public void Add_Should_SupportDifferentDelimiters( string inputValue, int expectedResult )
		{
			// Given
			var calculator = new StringCalculator( );

			// When
			var result = calculator.Add( inputValue );

			// Then
			Assert.Equal( expectedResult, result );
		}

		[Theory]
		[InlineData( "11,-12", "negatives not allowed -12" )]
		[InlineData( "-13,14,-15", "negatives not allowed -13,-15" )]
		public void Add_Should_ThrowAnException_WhenNumbersAreNegative( string inputValue, string expectedError )
		{
			// Given
			var calculator = new StringCalculator( );

			// When
			var exception = Record.Exception( ( ) => { calculator.Add( inputValue ); } );

			// Then
			Assert.NotNull( exception );
			Assert.Equal( expectedError, exception.Message );
		}

		private static string GenerateAnInputValueThatAddsUpTo( int total )
		{
			var random = new Random( );
			var numbers = new List<int>( );
			while ( total > 0 )
			{
				var randomNumber = random.Next( 1, 13 );
				if ( total < randomNumber )
				{
					numbers.Add( total );
					total = 0;
				}
				else
				{
					total -= randomNumber;
					numbers.Add( randomNumber );
				}
			}

			return string.Join( ',', numbers );
		}
	}
}
